import { defineRule } from "vee-validate";

defineRule("required", (value: string) => {
  if (value && value.trim()) {
    return true;
  }
  return "Fältet är obligatoriskt";
});
defineRule("isAvegaEmail", (value: string) => {
  if (value && value.includes("@") && value.split("@")[1] === "avega.se") {
    return true;
  }
  return "Måste vara en avega.se mailadress";
});
